using Xunit;
using System.Linq;
using Newtonsoft.Json;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Repositories;
using CarRent.Tests.Mocks;
using CarRent.Tests.TestBases;

namespace CarRent.Tests
{
    public class DataTest : DataBasedTest
    {
        // UnitTests
        [Fact]
        public void CreatesFileIfNoneExists()
        {
            _rental.LoadData(FileSystemMock.NullFile);
            _rental.SaveData();
            Assert.True(FileSystemMock.Object.File.Exists(FileSystemMock.TestFile), $"File {FileSystemMock.TestFile} has not been created.");
        }
        
        [Fact]
        public void LoadsDataIfFileExists()
        {
            _rental.LoadData(FileSystemMock.DummyDataFile);
            Assert.True(_rental.PersonRepository.Persons.FirstOrDefault().FullName == "Test Person", "Data was not properly loaded");
        }

        [Fact]
        public void DataIsSavedWhenAddingVehicleOrPerson()
        {
            _rental.LoadData(FileSystemMock.NullFile);
            _rental.PersonRepository.AddPerson<Customer>("Test", "Person");
            _rental.VehicleRepository.AddVehicle<Truck>("Test_Vehicle", 12);

            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            var json = FileSystemMock.Object.File.ReadAllText(FileSystemMock.TestFile);
            Repositoriy repositoriy = JsonConvert.DeserializeObject<Repositoriy>(json, settings);

            Assert.True(_rental.PersonRepository.Customers.First().FullName == "Test Person", "Person is not named 'Test Person'");
            Assert.True(_rental.VehicleRepository.Trucks.First().Name == "Test_Vehicle", "Varhicle is not named 'Test_Vehicle'");
        }
    }
}
