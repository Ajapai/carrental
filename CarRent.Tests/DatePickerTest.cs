using Xunit;
using System;
using CarRent.Models.Helper;

namespace CarRent.Tests
{
    public class DatePickerTest
    {
        DatePickerHelper _datePicker;
        DateTime _testTime = DateTime.ParseExact("20201007", "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).Date;

        public DatePickerTest()
        {
            _datePicker = new DatePickerHelper(_testTime);
        }

        // UnitTests
        [Fact]
        public void CannotPickYearBeforeEarliestDate()
        {
            _datePicker.Year -= 1;
            Assert.True(_datePicker.Date == _testTime);
        }

        [Fact]
        public void CannotPickNonexistantDates()
        {
            _datePicker.Month = 4;
            _datePicker.Day = 31;
            Assert.True(_datePicker.Day == 30);
        }

        [Fact]
        public void DatePickerAdjustsInvalidDates()
        {
            _datePicker.Month = 3;
            _datePicker.Day = 31;
            Assert.True(_datePicker.Day == 31);

            _datePicker.Month = 4;
            Assert.True(_datePicker.Day == 30);
        }

        [Fact]
        public void LeapYearIsIncluded()
        {
            _datePicker.Year = 2024;
            Assert.True(DateTime.IsLeapYear(_datePicker.Year));
            
            _datePicker.Month = 2;
            _datePicker.Day = 29;
            Assert.True(_datePicker.Day == 29);

            _datePicker.Year = 2025;
            Assert.True(_datePicker.Day == 28);
        }
    }
}