using CarRent.Models;
using CarRent.Tests.Mocks;

namespace CarRent.Tests.TestBases
{
    public class DataBasedTest
    {
        protected Rental _rental;

        // Constructor
        public DataBasedTest()
        {
            if(_rental == null)
            {
                _rental = Rental.InitializeWithSpecificFileSystem(FileSystemMock.SetUpFileSystem());
                _rental.LoadData(FileSystemMock.TestFile);
            }
        }  
    }
}