using Xunit;
using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Repositories;
using CarRent.Tests.TestBases;
using CarRent.Tests.Mocks;

namespace CarRent.Tests
{
    public class RepositoryTest : DataBasedTest
    {
        [Fact]
        public void AddPersonStoresTypeCorrectly()
        {
            PersonRepository personRepository = new PersonRepository{Persons = new List<Person>()};
            personRepository.AddPerson<Employee>("Test", "Employee");
            personRepository.AddPerson<Customer>("Test", "Customer");

            Assert.True(personRepository.Employees.First().FullName == "Test Employee", "Person was not added properly");
            Assert.True(personRepository.Customers.First().FullName == "Test Customer", "Person was not added properly");
        }

        [Fact]
        public void AddVehicleStoresTypeCorrectly()
        {
            VehicleRepository vehicleRepository = new VehicleRepository{Vehicles = new List<Vehicle>()};
            vehicleRepository.AddVehicle<Car>("Test_Car", 12);
            vehicleRepository.AddVehicle<Mofa>("Test_Mofa", 14);
            vehicleRepository.AddVehicle<Truck>("Test_Truck", 13);

            Assert.True(vehicleRepository.Cars.First().Name == "Test_Car", "Vehicle was not added properly");
            Assert.True(vehicleRepository.Mofas.First().Name == "Test_Mofa", "Vehicle was not added properly");
            Assert.True(vehicleRepository.Trucks.First().Name == "Test_Truck", "Vehicle was not added properly");
        }
    }
}