using Moq;
using Newtonsoft.Json;
using System.IO.Abstractions;
using System.Collections.Generic;
using CarRent.Models.Repositories;
using CarRent.Models.Persons;


namespace CarRent.Tests.Mocks
{
    public static class FileSystemMock
    {
        private static string _file;
        private static Mock<IFileSystem> _fileSystem;
        private static Dictionary<string, string> _files;
        public static IFileSystem Object { get => _fileSystem?.Object; }
        public static string TestFile { get => _file; }
        public static string NullFile 
        {
            get
            {
                if(FileSystemMock.Object.File.Exists(_file))
                {
                    FileSystemMock.Object.File.Delete(_file);
                }
                return _file;
            }
        }
        public static string DummyDataFile
        {
            get
            {
                Repositoriy Repositoriy = new Repositoriy
                {
                    PersonRepository = new PersonRepository(){Persons = new List<Person> {new Customer() {FirstName = "Test", LastName = "Person"}}}
                };
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                };
                var repoJson = JsonConvert.SerializeObject(Repositoriy, settings);
                FileSystemMock.Object.File.WriteAllText(_file, repoJson);
                return _file;
            }
        }
        public static IFileSystem SetUpFileSystem()
        {
            if(_fileSystem != null)
            {
                return _fileSystem.Object;
            }

            _file = "test_file";
            _files = new Dictionary<string, string>();
            _fileSystem = new Mock<IFileSystem>();

            // Read
            _fileSystem.Setup(
                f => f.File.ReadAllText(It.IsAny<string>())
            ).Returns<string>(name => _files[name]);

            // Write
            _fileSystem.Setup(
                f => f.File.WriteAllText(It.IsAny<string>(), It.IsAny<string>())
            ).Callback<string, string>((name, content) => _files[name] = content);

            // Exists
            _fileSystem.Setup(
                f => f.File.Exists(It.IsAny<string>())
            ).Returns<string>(name => _files.ContainsKey(name));

            // Delete
            _fileSystem.Setup(
                f => f.File.Delete(It.IsAny<string>())
            ).Callback<string>(name => _files.Remove(name));

            return _fileSystem.Object;
        }
    }
}