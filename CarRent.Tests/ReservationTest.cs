using Xunit;
using System;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Repositories;
using CarRent.Models.Administration;
using CarRent.Tests.TestBases;

namespace CarRent.Tests
{
    public class ReservationTest : DataBasedTest
    {
        // UnitTests
        [Fact]
        public void ReservationCanOnlyBeStoredIfDatesAreFree()
        {
            ReservationRepository repo = new ReservationRepository(){Reservations = new List<Reservation>()};
            DateTime now = DateTime.Now.Date;
            Car car = new Car{Name = "Subuwu", PricePerDay = 12};
            Reservation reservation1 = new Reservation(
                car,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(1),
                now.AddDays(2)
            );
            Reservation reservation2 = new Reservation(
                car,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(0),
                now.AddDays(3)
            );
            Assert.True(repo.AddReservation(reservation1), "Reservation could not be added.");
            Assert.False(repo.AddReservation(reservation2), "reservation was added.");
        }

        [Fact]
        public void ReservationsOnTheSameDayIsNotValid()
        {
            ReservationRepository repo = new ReservationRepository(){Reservations = new List<Reservation>()};
            DateTime now = DateTime.Now.Date;
            Car car = new Car{Name = "Subuwu", PricePerDay = 12};
            Reservation reservation1 = new Reservation(
                car,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(1),
                now.AddDays(2)
            );
            Reservation reservation2 = new Reservation(
                car,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(0),
                now.AddDays(1)
            );
            Assert.True(repo.AddReservation(reservation1), "Reservation could not be added.");
            Assert.False(repo.IsReservationFree(reservation2.Start, reservation2.End, reservation2.Vehicle), "reservation free");
        }

        [Fact]
        public void ReservationsOnDiffrentCarsValid()
        {
            ReservationRepository repo = new ReservationRepository(){Reservations = new List<Reservation>()};
            DateTime now = DateTime.Now.Date;
            Car car1 = new Car{Name = "Subuwu", PricePerDay = 12};
            Car car2 = new Car{Name = "Subawari", PricePerDay = 12};
            Reservation reservation1 = new Reservation(
                car1,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(1),
                now.AddDays(2)
            );
            Reservation reservation2 = new Reservation(
                car2,
                new Customer{FirstName = "Test", LastName = "Person"},
                now.AddDays(0),
                now.AddDays(1)
            );
            Assert.True(repo.AddReservation(reservation1), "Reservation could not be added.");
            Assert.True(repo.IsReservationFree(reservation2.Start, reservation2.End, reservation2.Vehicle), "reservation not free");
        }
    }
}