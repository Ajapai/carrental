using System;
using CarRent.Interfaces;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;

namespace CarRent.Models.Administration
{
    public class Reservation : IPrintable
    {
        // fields
        private bool _isReturned;
        private Customer _customer;
        private Vehicle _vehicle;
        private DateTime _creationDate;
        private DateTime _start;
        private DateTime _end;
        private int _price;

        // constructor
        public Reservation(Vehicle vehicle, Customer customer, DateTime start, DateTime end,
            bool isReturned = false, int? price = null, DateTime? creationDate = null)
        {
            _isReturned = isReturned;
            _customer = customer;
            _vehicle = vehicle;
            _start = start;
            _end = end;
            _creationDate = creationDate??DateTime.Now.Date;
            _price = price??(_end - _start).Days * _vehicle.PricePerDay;
        }

        // Properties
        public bool IsReturned { get => _isReturned; }
        public bool IsOverdue { get => _end < DateTime.Now.Date && !_isReturned; }
        public Customer Customer { get => _customer; }
        public Vehicle Vehicle { get => _vehicle; }
        public DateTime CreationDate { get => _creationDate; }
        public DateTime Start { get => _start; }
        public DateTime End { get => _end; }
        public int Price {get => _price; }
        public string DisplayName { get => $"{Vehicle.Name} | {StartAsString} - {EndAsString}{(IsOverdue ? " (Overdue)" : "")}";}
        public string StartAsString {get => $"{_start.Day.ToString().PadLeft(2, '0')}.{_start.Month.ToString().PadLeft(2, '0')}.{_start.Year}"; }
        public string EndAsString {get => $"{_end.Day.ToString().PadLeft(2, '0')}.{_end.Month.ToString().PadLeft(2, '0')}.{_end.Year}"; }

        // methods
        public void Return(){_isReturned = true; Rental.Instance.SaveData();}
        public bool IsInsideReservation(DateTime date)
        {
            return date <= _end && date >= _start;
        }
        public void Delete()
        {
            Rental.Instance.ReservationRepository.Reservations.Remove(this);
            Rental.Instance.SaveData();
        }
    }
}