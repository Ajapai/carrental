using System;
using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Administration;

namespace CarRent.Models.Repositories
{
    public class ReservationRepository
    {
        public List<Reservation> Reservations { get; set; }
        public List<Reservation> ActiveReservasions { get => Reservations.Where(r => !r.IsReturned && !r.IsOverdue).ToList(); }
        public List<Reservation> OverdueReservasions { get => Reservations.Where(r => r.IsOverdue).ToList(); }
        public List<Reservation> CustomerReservations(Customer customer, bool onlyActive = false)
        {
            return (onlyActive ? ActiveReservasions : Reservations).Where(r => r.Customer == customer).ToList();
        }
        public List<Reservation> VehicleReservations(Vehicle vehicle, bool onlyActive = false)
        {
            return (onlyActive ? ActiveReservasions : Reservations).Where(r => r.Vehicle == vehicle).ToList();
        }
        public bool AddReservation(Reservation reservation)
        {
            if(IsReservationFree(reservation.Start, reservation.End, reservation.Vehicle))
            {
                Reservations = Reservations.Append(reservation).OrderBy(r => r.Start).ToList();
                Rental.Instance.SaveData();
                return true;
            }
            return false;
        }
        public bool IsReservationFree(DateTime start, DateTime end, Vehicle vehicle)
        {
            return !VehicleReservations(vehicle, true).Any(r => 
                // True if start or end inside any reservation
                (r.IsInsideReservation(start) || r.IsInsideReservation(end)) ||
                // True if any reservastions start or end is inside new reservation
                (r.Start <= end && r.Start >= start) || (r.End <= end && r.Start >= start)
            );
        }
    }
}