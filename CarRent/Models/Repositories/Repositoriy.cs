namespace CarRent.Models.Repositories
{
    public class Repositoriy
    {
        public PersonRepository PersonRepository { get; set; }
        public VehicleRepository VehicleRepository { get; set; }
        public ReservationRepository ReservationRepository { get; set; }
    }
}