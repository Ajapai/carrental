using CarRent.Models.Vehicles;
using System.Collections.Generic;
using System.Linq;

namespace CarRent.Models.Repositories
{
    public class VehicleRepository
    {
        public List<Vehicle> Vehicles { get; set; }
        public List<Car> Cars { get => Vehicles.OfType<Car>().ToList();}
        public List<Mofa> Mofas { get => Vehicles.OfType<Mofa>().ToList(); }
        public List<Truck> Trucks { get => Vehicles.OfType<Truck>().ToList(); }

        public Vehicle AddVehicle<T>(string name, int pricePerDay)
            where T: Vehicle
        {
            var vehicleInstance = System.Activator.CreateInstance<T>();
            vehicleInstance.Name = name;
            vehicleInstance.PricePerDay = pricePerDay;

            Vehicles = Vehicles.Append(vehicleInstance).OrderBy(v => v.GetType().Name).ToList();
            Rental.Instance.SaveData();
            return vehicleInstance;
        }
        public void ToggleMofa(Mofa mofa)
        {
            if(mofa.HasPassengerSeat)
            {
                mofa.HasPassengerSeat = false;
            }
            else
            {
                mofa.HasPassengerSeat = true;
            }
            Rental.Instance.SaveData();
        }
        public void ChangeSeats(Car car, int seats)
        {
            car.Seats = seats;
            Rental.Instance.SaveData();
        }
        public void ChangeHold(Truck truck, int hold)
        {
            truck.HoldInKg = hold;
            Rental.Instance.SaveData();
        }

        public void DeleteVehicle(int index)
        {
            Vehicles.Remove(Vehicles[index]);
            Rental.Instance.SaveData();
        }
    }
}