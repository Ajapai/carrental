using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Persons;

namespace CarRent.Models.Repositories
{
    public class PersonRepository
    {
        public List<Person> Persons { get; set; }
        public List<Customer> Customers {get => Persons.OfType<Customer>().ToList();}
        public List<Employee> Employees {get => Persons.OfType<Employee>().ToList();}

        public Person AddPerson<T>(string first, string last)
            where T: Person
        {
            var personInstance = System.Activator.CreateInstance<T>();
            personInstance.FirstName = first;
            personInstance.LastName = last;

            Persons = Persons.Append(personInstance).OrderBy(p => p.GetType().Name).ToList();
            Rental.Instance.SaveData();
            return personInstance;
        }
        public void DeletePerson(int index)
        {
            Persons.Remove(Persons[index]);
            Rental.Instance.SaveData();
        }

        public void ToggleEmployee(Employee employee)
        {
            if(employee.IsFree)
            {
                employee.IsFree = false;
            }
            else
            {
                employee.IsFree = true;
            }
            Rental.Instance.SaveData();
        }
        public void ToggleCustomer(Customer customer)
        {
            if(customer.IsServed)
            {
                customer.IsServed = false;
            }
            else
            {
                customer.IsServed = true;
            }
            Rental.Instance.SaveData();
        }
        public void SetAllFree()
        {
            foreach (Person person in Persons)
            {
                if(person is Employee employee && !employee.IsFree)
                {
                    ToggleEmployee(employee);
                }
                else if(person is Customer customer && customer.IsServed)
                {
                    ToggleCustomer(customer);
                }
            }
        }
    }
}