using CarRent.Models.Vehicles;

namespace CarRent.Models.Persons
{
   public class Employee : Person
   {
      public bool IsFree { get; set; } = true;
      public override string DisplayName 
      { 
         get => FullName + " - employee - " + (IsFree ? "free" : "busy");
      }
      public bool RepairVehicle(Vehicle vehicle)
      {
         return vehicle.Repair();
      }
      public bool CleanVehicle(Vehicle vehicle)
      {
         return vehicle.Clean();
      }
   }
}