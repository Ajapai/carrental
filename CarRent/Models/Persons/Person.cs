using CarRent.Interfaces;
using CarRent.Models.Vehicles;

namespace CarRent.Models.Persons
{
    public abstract class Person : IPrintable
    {
        public abstract string DisplayName { get; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get => FirstName + " " + LastName; }
        public bool? DriveVehicle(Vehicle vehicle)
        {
            return vehicle.DriveAround();
        }
    }
}