namespace CarRent.Models.Persons
{
   public class Customer : Person
   {
      public bool IsServed { get; set; } = false;
      public override string DisplayName { get => FullName + " - customer - " + (IsServed ? "served" : "waiting" );}
   }
}