using System;
using CarRent.Interfaces;

namespace CarRent.Models.Vehicles
{
   public abstract class Vehicle : IPrintable
   {
      private bool _isDamaged;
      public bool IsDamaged { get => _isDamaged; }
      private bool _isDirty;
      public bool IsDirty { get => _isDirty; }
      public string Name { get; set; }
      public int PricePerDay { get; set; }
      protected abstract string Specs { get; }
      public string DisplayName { get => $"{Specs.PadRight(42, ' ')} | {PricePerDay}.- per day."; }

      public bool Repair()
      {
         if(_isDamaged)
         {
            _isDamaged = false;
            return true;
         }
         return false;
      }
      public bool Clean()
      {
         if(_isDirty)
         {
            _isDirty = false;
            return true;
         }
         return false;
      }
      public bool? DriveAround()
      {
         Random random = new Random();
         int randomEvent = random.Next(10);
         if(randomEvent <= 3)
         {
            _isDirty = true;
            if(randomEvent == 0)
            {
               _isDamaged = true;
               return null;
            }
            return false;
         }
         return true;
      }
   }
}