namespace CarRent.Models.Vehicles
{
   public class Truck : Vehicle
   {
      public int HoldInKg { get; set; } = 1000;
      protected override string Specs 
      {
         get => $"{Name} is a {HoldInKg}kg holding truck.";
      }
   } 
}