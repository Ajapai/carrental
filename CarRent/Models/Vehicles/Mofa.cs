namespace CarRent.Models.Vehicles
{
    public class Mofa : Vehicle
    {
       public bool HasPassengerSeat { get; set; } = false;
        protected override string Specs 
        { 
            get => $"{Name} is a {(HasPassengerSeat ? "two seater" : "one seater")} mofa.";
        }
    } 
}