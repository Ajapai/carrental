namespace CarRent.Models.Vehicles
{
    public class Car : Vehicle
    {
        public int Seats { get; set; } = 4;
        protected override string Specs 
        {
            get => $"{Name} is a {Seats}-seater car.";
        }
    } 
}