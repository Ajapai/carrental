using Newtonsoft.Json;
using System.IO.Abstractions;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Repositories;
using CarRent.Models.Administration;

namespace CarRent.Models
{
    public sealed class Rental
    {
        private string _loadedFile;
        readonly IFileSystem _fileSystem;
        private static Rental _instance = null;
        private Rental(IFileSystem fs)
        {
            _fileSystem = fs;
        }
        public static Rental InitializeWithSpecificFileSystem(IFileSystem fs)
        {
            if(_instance == null)
            {
                _instance = new Rental(fs);
            }
            return _instance;
        }
        public  static Rental Instance 
        { 
            get
            {
                if(_instance == null)
                {
                    _instance = new Rental(new FileSystem());
                }
                return _instance;
            }
        }

        public Repositoriy Repositoriy { get; set; }
        public PersonRepository PersonRepository 
        { 
            get => Repositoriy.PersonRepository; 
            set =>  Repositoriy.PersonRepository = value; 
        }
        public VehicleRepository VehicleRepository 
        { 
            get => Repositoriy.VehicleRepository; 
            set =>  Repositoriy.VehicleRepository = value; 
        }
        public ReservationRepository ReservationRepository 
        { 
            get => Repositoriy.ReservationRepository; 
            set =>  Repositoriy.ReservationRepository = value; 
        }

        public void SaveData()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            var repoJson = JsonConvert.SerializeObject(Repositoriy, settings);
            _fileSystem.File.WriteAllText(_loadedFile, repoJson);
        }

        public void LoadData(string file = "carRent_saveData.txt")
        {
            _loadedFile = file;
            if(_fileSystem.File.Exists(_loadedFile))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                };
                var json = _fileSystem.File.ReadAllText(_loadedFile);
                Repositoriy = JsonConvert.DeserializeObject<Repositoriy>(json, settings);
            }
            else
            {
                Repositoriy = new Repositoriy();
                PersonRepository = new PersonRepository(){Persons = new List<Person>()};
                VehicleRepository = new VehicleRepository(){Vehicles = new List<Vehicle>()};
                ReservationRepository = new ReservationRepository(){Reservations = new List<Reservation>()};
            }
        }
        public void Reload()
        {
            Repositoriy = null;
            Navigation.Reset();
            LoadData();
            PersonRepository.SetAllFree();
        }
    }
}