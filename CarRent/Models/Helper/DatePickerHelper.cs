using System;

namespace CarRent.Models.Helper
{
    public class DatePickerHelper
    {
        private int _index;
        private DateTime _earliestDate;
        private int[] _dateArray;
        public DatePickerHelper(DateTime date)
        {
            _index = 0;
            _earliestDate = date.Date;
            _dateArray = CreateIntArray(date);
        }

        public int Day 
        { 
            get => _dateArray[0] * 10 + _dateArray[1]; 
            set
            {
                if(value < 1)
                {
                    value = 1;
                }
                if(value > GetDaysForMonth())
                {
                    value = GetDaysForMonth();
                }
                _dateArray[0] = value / 10;
                _dateArray[1] = value % 10;
            }
        }
        public int Month 
        {
            get => _dateArray[3] * 10 + _dateArray[4];
            set
            {
                if(value < 1)
                {
                    value = 1;
                }
                if(value > 12)
                {
                    value = 12;
                }
                _dateArray[3] = value / 10;
                _dateArray[4] = value % 10;
                AdjustDays();
            }
        }
        public int Year 
        { 
            get => _dateArray[6] * 1000 + _dateArray[7] * 100 + _dateArray[8] * 10 + _dateArray[9];
            set
            {
                if(value > 9999)
                {
                    value = 9999;
                }
                if(value < _earliestDate.Year)
                {
                    value = _earliestDate.Year;
                }
                _dateArray[6] = value / 1000; 
                _dateArray[7] = value % 1000 / 100;
                _dateArray[8] = value % 1000 % 100 / 10;
                _dateArray[9] = value % 1000 % 100 % 10;
                AdjustDays();
            }
        }
        public string DateAsString {get => $"{Day.ToString().PadLeft(2, '0')}.{Month.ToString().PadLeft(2, '0')}.{Year}"; }
        public DateTime Date 
        { 
            get => DateTime.ParseExact(
                $"{Year}{Month.ToString().PadLeft(2, '0')}{Day.ToString().PadLeft(2,'0')}", 
                "yyyyMMdd", 
                System.Globalization.CultureInfo.InvariantCulture
            );
        }


        public DateTime ChooseDate(string dateMessage)
        {
            Console.WriteLine($"Plese select the {dateMessage}: (Use arrow keys)");
            Console.WriteLine(DateAsString);
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            while(true)
            {
                switch(Console.ReadKey(true).Key)
                {
                    case ConsoleKey.LeftArrow:
                        if(Console.CursorLeft > 0)
                        {
                            _index = Console.CursorLeft - 1;
                            Console.SetCursorPosition(_index, Console.CursorTop);
                        }
                        break;

                    case ConsoleKey.RightArrow:
                        if(Console.CursorLeft < 9)
                        {
                            _index = Console.CursorLeft + 1;
                            Console.SetCursorPosition(_index, Console.CursorTop);
                        }
                        break;

                    case ConsoleKey.UpArrow:
                        Add();
                        UpdateConsole();
                        break;

                    case ConsoleKey.DownArrow:
                        Take();
                        if(_earliestDate > Date)
                        {
                            _dateArray = CreateIntArray(_earliestDate);
                        }
                        UpdateConsole();
                        break;

                    case ConsoleKey.Enter:
                        Console.SetCursorPosition(0, Console.CursorTop + 1);
                        return Date;

                    default:
                        break;
                }
            }
        }

        private void Add()
        {
            switch(_index)
            {
                case 0:
                    Day += 10;
                    break;
                case 1:
                    Day += 1;
                    break;
                case 3:
                    Month += 10;
                    break;
                case 4:
                    Month += 1;
                    break;
                case 6: 
                    Year += 1000;
                    break;
                case 7: 
                    Year += 100;
                    break;
                case 8:
                    Year += 10;
                    break;
                case 9:
                    Year += 1;
                    break;
                default:
                    break;
            }
        }

        private void Take()
        {
            switch(_index)
            {
                case 0:
                    Day -= 10;
                    break;
                case 1:
                    Day -= 1;
                    break;
                case 3:
                    Month -= 10;
                    break;
                case 4:
                    Month -= 1;
                    break;
                case 6: 
                    Year -= 1000;
                    break;
                case 7: 
                    Year -= 100;
                    break;
                case 8:
                    Year -= 10;
                    break;
                case 9:
                    Year -= 1;
                    break;
                default:
                    break;
            }
        }

        private int GetDaysForMonth()
        {
            switch(Month)
            {
                case 2:
                    return DateTime.IsLeapYear(Year) ? 29 : 28;
                case 4: case 6: case 9: case 11:
                    return 30;
                case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                    return 31;
                default:
                    return 0;
            }
        }

        private int[] CreateIntArray(DateTime date)
        {
            return new int[] 
            {
                date.Day / 10,
                date.Day % 10,
                -1,
                date.Month / 10,
                date.Month % 10,
                -1,
                date.Year / 1000,
                date.Year % 1000 / 100,
                date.Year % 1000 % 100 / 10,
                date.Year % 1000 % 100 % 10
            };
        }

        private void AdjustDays()
        {
            if(Day > GetDaysForMonth())
            {
                Day = GetDaysForMonth();
            }
        }

        private void UpdateConsole()
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(DateAsString);
            Console.SetCursorPosition(_index, Console.CursorTop);
        }
    }
}