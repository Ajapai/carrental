using System;
using System.Collections.Generic;
using CarRent.Interfaces;

namespace CarRent.Models.Helper
{
    public static class ConsoleHelper
    {
        public static int ReadRange(uint max, uint min = 0)
        {
            int key = Console.ReadKey(true).KeyChar - '0';
            if(key < min || key > max)
            {
                Console.WriteLine($"\nPlease press a key between {min} and {max}.");
            }
            while(key < min || key > max)
            {
                key = Console.ReadKey(true).KeyChar - '0';
            }
            return key;
        }

        public static int ReadLineRange(uint max, uint min = 0)
        {
            // Max integer value
            if(max > 2147483647)
            {
                max = 2147483647;
            }
            int input;
            do
            {
                Console.Write("Please enter a number between " + min.ToString() + " and " + max.ToString() + " : ");
                try
                {
                    input = Convert.ToInt32(Console.ReadLine());
                }
                catch(FormatException)
                {
                    input = -1;
                }
                catch(OverflowException)
                {
                    input = -1;
                }
            }while(input < min || input > max);
            return input;
        }
        
        public static DateTime ReadDateTime(string dateMessage, DateTime? start = null)
        {
            DatePickerHelper dateHelper = new DatePickerHelper(start??DateTime.Now);
            return dateHelper.ChooseDate(dateMessage);
        }

        public static void PrintList<T>(List<T> list, bool enumerate = true)
            where T: IPrintable
        {
            for(int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{(enumerate?$"[{i + 1}] - " : "")}{list[i].DisplayName}");
            };
            Console.Write("\n");
        }

        public static void Confirmation(IPrintable iPrintable = null)
        {
            Console.WriteLine($"\n{(iPrintable != null ? $"\nSuccessfully added:\n{iPrintable.DisplayName}\n\n" : "")}Press any key to continue...");
            Console.ReadKey(true);
        }
        
    }
}