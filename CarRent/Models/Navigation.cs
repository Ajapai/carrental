using System;
using CarRent.Models.Menus;

namespace CarRent.Models
{
    public static class Navigation
    {
        private static MainMenu _mainMenu = new MainMenu();
        public static MainMenu MainMenu { get => _mainMenu; }
        private static ShopMenu _shopMenu = new ShopMenu();
        public static ShopMenu ShopMenu { get => _shopMenu; }
        private static WorkshopMenu _workshopMenu = new WorkshopMenu();
        public static WorkshopMenu WorkshopMenu { get => _workshopMenu; }
        private static VehicleMenu _vehicleMenu = new VehicleMenu();
        public static VehicleMenu VehicleMenu { get => _vehicleMenu; }
        private static PersonMenu _personMenu = new PersonMenu();
        public static PersonMenu PersonMenu { get => _personMenu; }
        private static StatisticsMenu _statisticsMenu = new StatisticsMenu();
        public static StatisticsMenu StatisticsMenu { get => _statisticsMenu; }
        public static bool IsFinished { get; set; }
        
        public static void Execute(Action callback)
        {
            if(callback == null)
            {
                return;
            }

            IsFinished = false;
            while(!IsFinished)
            {
                callback();
            }
        }

        public static void Reset()
        {
            _mainMenu = new MainMenu();
            _shopMenu = new ShopMenu();
            _personMenu = new PersonMenu();
            _vehicleMenu = new VehicleMenu();
            _workshopMenu = new WorkshopMenu();
            _statisticsMenu = new StatisticsMenu();
        }
    }
}