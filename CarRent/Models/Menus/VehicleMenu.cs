using System;
using System.Collections.Generic;
using CarRent.Models.Vehicles;
using CarRent.Models.Helper;
using CarRent.Exceptions;

namespace CarRent.Models.Menus
{      
    public class VehicleMenu : Menu
    {
        public VehicleMenu() : base("Vehicle Menu")
        {
            _options = new List<string>()
            {
                "[1] - Add Vehicle",
                "[2] - Edit Vehicle",
                "[3] - Change Price",
                "[4] - Delete Vehicle",
                "[0] - Return to Main Menu"
            };
            _actions = new List<Action>()
            {
                () => Navigation.IsFinished = true,
                () => Add(),
                () => Loop((string description) => Edit(description), 
                    "Please choose a car to change number of seats,\n a mofa to toggle passenger seat,\n a truck to change hold or [0] to stop:\n"),
                () => Loop((string description) => ChangePrice(description), 
                    "Please choose a vehicle to change the price per day or [0] to stop\n"),
                () => Loop((string description) => Delete(description), 
                    "Please choose a vehicle to Delete or [0] to stop:\n")
            };
        }

        // Fields
        private int _choice;

        
        // Callback functions
        private void Add()
        {
            Console.Write("Please enter the Name of the Vehicle: ");
            string vehicleName = Console.ReadLine();
            Console.WriteLine("Please enter price per day.");
            int price = ConsoleHelper.ReadLineRange(100,1);
            Console.Write("Is this vehicle:\n[1] - a car\n[2] - a mofa\n[3] - a truck");
            switch(ConsoleHelper.ReadRange(3,1))
            {
                case 1:
                    ConsoleHelper.Confirmation(Rental.Instance.VehicleRepository.AddVehicle<Car>(vehicleName, price));
                    break;
                case 2:
                    ConsoleHelper.Confirmation(Rental.Instance.VehicleRepository.AddVehicle<Mofa>(vehicleName, price));
                    break;
                case 3:
                    ConsoleHelper.Confirmation(Rental.Instance.VehicleRepository.AddVehicle<Truck>(vehicleName, price));
                    break;
            }
        }

        private void Edit(string description)
        {
            Vehicle vehicle = Rental.Instance.VehicleRepository.Vehicles[_choice - 1];
            if(vehicle is Car car)
            {
                Console.WriteLine("How many Seats does this car have?");
                Rental.Instance.VehicleRepository.ChangeSeats(car, ConsoleHelper.ReadLineRange(10,2));
            }
            else if(vehicle is Mofa mofa)
            {
                Rental.Instance.VehicleRepository.ToggleMofa(mofa);
            }
            else if(vehicle is Truck truck)
            {
                Console.WriteLine("How much hold in kg does this truck have?");
                Rental.Instance.VehicleRepository.ChangeHold(truck, ConsoleHelper.ReadLineRange(2000,100));
            }
            else
            {
                throw new DerivedClassException($"Derived class {vehicle.GetType()} has to be implemented here.");
            }

            Repeat(description);
        }

        private void ChangePrice(string description)
        {
            Vehicle vehicle = Rental.Instance.VehicleRepository.Vehicles[_choice - 1];
            Console.WriteLine("How much does this vehicle cost per day?");
            vehicle.PricePerDay = ConsoleHelper.ReadLineRange(100,1);            
            Repeat(description);
        }

        private void Delete(string description)
        {
            Draw();
            string vehicleName = Rental.Instance.VehicleRepository.Vehicles[_choice - 1].Name;
            Console.WriteLine($"Are you sure you want to delete {vehicleName}?\n\n[1] - Yes!\n[0] - No!");
            if(ConsoleHelper.ReadRange(1) == 1)
            {
                Rental.Instance.VehicleRepository.DeleteVehicle(_choice - 1);
                Console.WriteLine($"\nSuccessfully deleted {vehicleName}.");
                ConsoleHelper.Confirmation();
            }
            else
            {
                Repeat(description);
            }
        }

        protected void Loop(Action<string> action, string description)
        {
            Console.WriteLine(description);
            ConsoleHelper.PrintList<Vehicle>(Rental.Instance.VehicleRepository.Vehicles);
            _choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.VehicleRepository.Vehicles.Count);
            while(_choice != 0)
            {
                action(description);
            }
        }


        // Helper functions
        private void Repeat(string description)
        {
            Draw();
            Console.WriteLine(description);
            ConsoleHelper.PrintList<Vehicle>(Rental.Instance.VehicleRepository.Vehicles);
            _choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.VehicleRepository.Vehicles.Count);
        }
    }
}