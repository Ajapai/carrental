using System;
using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Administration;
using CarRent.Models.Helper;

namespace CarRent.Models.Menus
{      
    public class ShopMenu : Menu
    {
        public ShopMenu() : base("Shop Menu")
        {
            _options = new List<string>()
            {
                "[1] - Select / change customer",
                "[2] - Select / change staff member",
                "[3] - Rent a vehicle",
                "[4] - Return vehicle / End reservation",
                "[5] - View customer reservations",
                "[6] - Selected customer leaves",
                "[0] - Exit to main menu"
            };
            _actions = new List<Action>()
            {
                () => Navigation.IsFinished = true,
                () => SelectCustomer(),
                () => SelectEmployee(),
                () => StartTransaction(),
                () => ReturnVehicle(),
                () => ShowReservations(),
                () => CustomerLeaves()
            };
        }

        // Fields & Properties
        private int _choice;
        private Customer _currentCustomer;
        private Dictionary<Customer, Employee> _counters = new Dictionary<Customer, Employee>();
        public Employee CurrentEmployee { get => _counters[_currentCustomer];}


        // Selection of customer and emplyee
        private void SelectCustomer()
        {
            Console.WriteLine("Choose a Customer or [0] to stop:");
            ConsoleHelper.PrintList<Customer>(Rental.Instance.PersonRepository.Customers);
            _choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.PersonRepository.Customers.Count);
            if(_choice == 0)
            {
                return;
            }
            _currentCustomer = Rental.Instance.PersonRepository.Customers[_choice - 1];
            if(!_counters.ContainsKey(_currentCustomer))
            {
                _counters[_currentCustomer] = null;
            }
        }
        private void SelectEmployee()
        {
            if(_currentCustomer == null)
            {
                Console.WriteLine("Please select a customer first. Press any key to continue...");
                Console.ReadKey(true);
                return;
            }
            Console.WriteLine("Choose a free employee or [0] to stop:");
            List<Employee> freeStaff = Rental.Instance.PersonRepository.Employees.Where(e => e.IsFree).ToList();
            ConsoleHelper.PrintList<Employee>(freeStaff);
            _choice = ConsoleHelper.ReadLineRange((uint)freeStaff.Count);
            if(_choice == 0)
            {
                return;
            }
            if(CurrentEmployee != null)
            {
                CurrentEmployee.IsFree = true;
            }
            _counters[_currentCustomer] = freeStaff[_choice - 1];
            _currentCustomer.IsServed = true;
            CurrentEmployee.IsFree = false;
        }
        private void CustomerLeaves()
        {
            if(_currentCustomer == null)
            {
                Console.WriteLine("Please select a customer who should leave first. Press any key to continue...");
                Console.ReadKey(true);
                return;
            }
            if(CurrentEmployee != null)
            {
                CurrentEmployee.IsFree = true;
            }
            _counters.Remove(_currentCustomer);
            _currentCustomer.IsServed = false;
            _currentCustomer = null;
        }


        // Transaction
        private void StartTransaction()
        {
            // Null Check
            if(_currentCustomer == null || CurrentEmployee == null)
            {
                Console.WriteLine("Please choose a customer and emplyee first. Press any key to continue...");
                Console.ReadKey(true);
                return;
            }

            // Choose type of vehicle
            Console.WriteLine("What kind of vehicle do you wish to rent?\n[1] - Car\n[2] - Mofa\n[3] - Truck\n[0] - None\n");
            List<Vehicle> vehicles = new List<Vehicle>();
            string type = "";
            switch (ConsoleHelper.ReadRange(3))
            {
                case 1:
                    vehicles = Rental.Instance.VehicleRepository.Cars.Select(c => (Vehicle)c).ToList();
                    type = "car";
                    break;
                case 2:
                    vehicles = Rental.Instance.VehicleRepository.Mofas.Select(c => (Vehicle)c).ToList();
                    type = "mofa";
                    break;
                case 3:
                    vehicles = Rental.Instance.VehicleRepository.Trucks.Select(c => (Vehicle)c).ToList();
                    type = "truck";
                    break;
                case 0:
                    return;
            }

            // Choose vehicle
            Draw();
            Console.WriteLine($"Please choose a {type} or [0] to stop:");
            ConsoleHelper.PrintList<Vehicle>(vehicles);
            _choice = ConsoleHelper.ReadLineRange((uint)vehicles.Count);
            if(_choice == 0)
            {
                return;
            }
            Vehicle choosenVehicle = vehicles[_choice - 1];

            // Choose time
            bool done = false;
            Reservation reservation = null;
            while(!done)
            {
                Draw();
                if(Rental.Instance.ReservationRepository.VehicleReservations(choosenVehicle, true).Count > 0)
                {
                    Console.WriteLine("Active reservations on vehicle:");
                    ConsoleHelper.PrintList<Reservation>(Rental.Instance.ReservationRepository.VehicleReservations(choosenVehicle, true), false);
                }
                else
                {
                    Console.WriteLine("Currently no other active reservations on this vehicle.\n");
                }
                DateTime startDate = ConsoleHelper.ReadDateTime("start date");
                DateTime endDate = ConsoleHelper.ReadDateTime("end date", startDate.AddDays(1));
                reservation = new Reservation(choosenVehicle, _currentCustomer, startDate, endDate);
                if(Rental.Instance.ReservationRepository.IsReservationFree(reservation.Start, reservation.End, reservation.Vehicle))
                {
                    Draw();
                    Console.WriteLine("Are you happy with the following reservation?\n");
                    Console.WriteLine($"Vehicle: {choosenVehicle.Name}\nStarting date: {reservation.StartAsString}" +
                        $"\nEnding date: {reservation.EndAsString}\nPrice: {reservation.Price}.-,\n");
                    Console.WriteLine("[1] - Yes!\n[0] - No!");
                    if(ConsoleHelper.ReadRange(1) == 1)
                    {
                        done = true;
                    }
                    else
                    {
                        Draw();
                        Console.WriteLine("Choose date again?\n[1] - Yes!\n[0] - No!");
                        if(ConsoleHelper.ReadRange(1) == 0)
                        {
                            return;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\nChoosen dates are not free, do you wanna try again?\n[1] - Yes!\n[0] - No!");
                    if(ConsoleHelper.ReadRange(1) == 0)
                    {
                        return;
                    }
                }
            }

            // Payment
            Draw();
            Console.WriteLine($"How do you want to pay the total price of {reservation.Price}.-?");
            Console.WriteLine("\n[1] - Cash\n[2] - Card");
            if(ConsoleHelper.ReadRange(2,1) == 1)
            {
                Console.WriteLine("\nHow much cash do you want to give?");
                int cash = ConsoleHelper.ReadLineRange((uint)(Math.Pow(10, Math.Floor(Math.Log10(reservation.Price) + 1)) * 2));
                while(cash < reservation.Price)
                {
                    Draw();
                    Console.WriteLine($"{cash}.- is not enough.. It costs {reservation.Price}.- in total.");
                    Console.WriteLine("\n[1] - Give more money\n[0] - Cancel the reservation");
                    if(ConsoleHelper.ReadRange(1) == 0)
                    {
                        Draw();
                        Console.WriteLine($"Thanks anyways, here's your money back ({cash}.-)");
                        Console.WriteLine("\n[1] - Sorry, I still have more money\n[0] - Take money and leave");
                        if(ConsoleHelper.ReadRange(1) == 0)
                        {
                            return;
                        }
                    }

                    Draw();
                    Console.WriteLine($"You currently paid {cash}.- out of {reservation.Price}.-\n\nHow much more do you want to give?");
                    cash += ConsoleHelper.ReadLineRange((uint)(Math.Pow(10, Math.Floor(Math.Log10(reservation.Price) + 1)) * 2));
                }

                if(cash > reservation.Price)
                {
                    Draw();
                    Console.WriteLine($"That makes {cash - reservation.Price}.- of change.");
                }
            }
            if(Rental.Instance.ReservationRepository.AddReservation(reservation))
            {
                Console.WriteLine("\nThanks for your payment, here are the keys and your receipt!" + 
                    $"\n(Theese keys are magical, they only work from {reservation.StartAsString} to {reservation.EndAsString}.)");
            }
            else
            {
                Console.WriteLine("Sorry, the choosen dates are no longer free");
            }
            ConsoleHelper.Confirmation();
        }


        //Administration
        private void ReturnVehicle()
        {
            // Null Check
            if(_currentCustomer == null || CurrentEmployee == null)
            {
                Console.WriteLine("Please select a customer and emplyee first. Press any key to continue...");
                Console.ReadKey(true);
                return;
            }
            List<Reservation> reservations = Rental.Instance.ReservationRepository.CustomerReservations(_currentCustomer)
                .Where(r => !r.IsReturned).OrderBy(r => r.Vehicle.Name).ToList();
            if(reservations.Count == 0)
            {
                Console.WriteLine($"{_currentCustomer.FirstName} has no active reservations..");
                ConsoleHelper.Confirmation();
                return;
            }
            Console.WriteLine("Choose a Vehicle to return or [0] to stop:");
            ConsoleHelper.PrintList<Reservation>(reservations);
            _choice = ConsoleHelper.ReadLineRange((uint)reservations.Count);
            if(_choice == 0)
            {
                return;
            }
            Reservation reservation = reservations[_choice - 1];
            Draw();
            Console.WriteLine($"Are you sure you want to end this reservation?\n({reservation.DisplayName})\n\n[1] - Yes!\n[0] - No!");
            if(ConsoleHelper.ReadRange(1) == 1)
            {
                reservation.Return();
                Console.WriteLine($"\nSucessfully returned {reservation.Vehicle.Name}");
                if(reservation.Vehicle.IsDirty && CurrentEmployee.CleanVehicle(reservation.Vehicle))
                {
                    Console.WriteLine($"({reservation.Vehicle.Name} was dirty and got cleaned by {CurrentEmployee.FirstName})");
                }
                ConsoleHelper.Confirmation();
            }            
        }

        private void ShowReservations()
        {
            if(_currentCustomer == null)
            {
                Console.WriteLine("Please select a customer first. Press any key to continue...");
                Console.ReadKey(true);
                return;
            }

            List<Reservation> reservations = Rental.Instance.ReservationRepository.CustomerReservations(_currentCustomer)
                .OrderBy(r => r.Vehicle.Name).ToList();
            if(reservations.Count == 0)
            {
                Console.WriteLine($"{_currentCustomer.FullName} has no past reservations.");
            }
            else
            {
                Console.WriteLine($"Reservations of {_currentCustomer.FullName}:\n");
                foreach (Reservation reservation in Rental.Instance.ReservationRepository.CustomerReservations(_currentCustomer))
                {
                    Console.WriteLine($"{reservation.Vehicle.Name} | {reservation.StartAsString} - {reservation.EndAsString} | " +
                        $"{reservation.Price}.- {(reservation.IsReturned ? "(Returned)" : reservation.IsOverdue ? "(Overdue)" : "")}");
                }
            }
            ConsoleHelper.Confirmation();
        }
        // Override functions
        public override void Draw()
        {
            Console.Clear();
            DrawBorder('╔', '╗');
            DrawTitle();
            DrawBorder('╠', '╣');
            foreach(string option in _options)
            {
                DrawLine(option);
            }
            if(_currentCustomer != null)
            {
                DrawBorder('╠', '╣');
                DrawLine($"Customer: {_currentCustomer.FullName}");
                DrawLine($"Employee: {CurrentEmployee?.FullName??"None"}");
            }
            DrawBorder('╚', '╝');
        }
    }
}