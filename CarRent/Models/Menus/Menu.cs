using System;
using System.Collections.Generic;

namespace CarRent.Models.Menus
{      
    public abstract class Menu
    {
        protected Menu(string title)
        {
            _title = title;
        }

        // Fields and properties
        protected readonly string _title;
        protected List<Action> _actions;
        protected List<string> _options;
        protected int Lenght
        {
            get{
                int lenght = 0;
                foreach(string option in _options)
                {
                    if(lenght < option.Length)
                    {
                        lenght = option.Length;
                    }
                }
                return lenght + 1;
            }
        }
        public int Count { get => _actions.Count; }

        // Menu drawing
        public virtual void Draw()
        {
            Console.Clear();
            DrawBorder('╔', '╗');
            DrawTitle();
            DrawBorder('╠', '╣');
            foreach(string option in _options)
            {
                DrawLine(option);
            }
            DrawBorder('╚', '╝');
        }

        protected void DrawBorder(char start, char end)
        {
            string line = start.ToString();
            for(int i = 0; i < Lenght; i++)
            {
                line += "═";
            }
            line += end;
            Console.WriteLine(line);
        }

        protected void DrawLine(string content = "")
        {
            string line = "║";
            line += content.PadRight(Lenght, ' ');
            line += "║";
            Console.WriteLine(line);
        }

        protected void DrawTitle()
        {
            DrawLine();
            DrawLine(_title.PadLeft(_title.Length + (Lenght - _title.Length) / 2, ' '));
            DrawLine();
        }

        // Helper functions
        public Action GetCallback(int choice)
        {
            try
            {
                return _actions[choice];
            }
            catch(IndexOutOfRangeException)
            {
                return null;
            }
        }
    }
}