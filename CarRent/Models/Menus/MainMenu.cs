using System;
using System.Collections.Generic;
using CarRent.Models.Helper;

namespace CarRent.Models.Menus
{      
    public class MainMenu : Menu
    {
        public MainMenu() : base("Main Menu")
        {
            _options = new List<string>()
            {
                "[1] - Enter Shop",
                "[2] - Enter Workshop",
                "[3] - Manage Persons",
                "[4] - Manage Vehicles",
                "[5] - Watch Statistics",
                "[0] - Exit Programm"
            };
            _actions = new List<Action>()
            {
                () => Exit(),
                () => GoTo(Navigation.ShopMenu),
                () => GoTo(Navigation.WorkshopMenu),
                () => GoTo(Navigation.PersonMenu),
                () => GoTo(Navigation.VehicleMenu),
                () => GoTo(Navigation.StatisticsMenu)
            };
        }

        // Using this method in a loop like `Navigation.Execute` results in the change of menu.
        private void GoTo(Menu menu)
        {
            menu.Draw();
            menu.GetCallback(ConsoleHelper.ReadRange((uint)menu.Count - 1))();
        }

        private void Exit()
        {
            Console.WriteLine("Are you sure you want to quit?\n\n[1] - Yes!\n[0] - No!");
            if(ConsoleHelper.ReadRange(1) == 1)
            {
                Rental.Instance.SaveData();
                Environment.Exit(0);
            }
            Navigation.IsFinished = true;
        }
    }
}