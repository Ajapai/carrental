using System;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Helper;
using CarRent.Exceptions;

namespace CarRent.Models.Menus
{      
    public class PersonMenu : Menu
    {
        public PersonMenu() : base("Person Menu")
        {
            _options = new List<string>()
            {
                "[1] - Add Person",
                "[2] - Edit Person",
                "[3] - Delete Person",
                "[0] - Return to Main Menu"
            };
            _actions = new List<Action>()
            {
                () => Navigation.IsFinished = true,
                () => Add(),
                () => Loop((string description) => Edit(description), "Please choose a person to toggle state or [0] to stop:\n"),
                () => Loop((string description) => Delete(description), "Please choose a person to delete or [0] to stop:\n")
            };
        }

        // Fields
        private int _choice;


        // Callback funcions
        private void Add()
        {
            Console.Write("Please enter the First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Please enter the Last Name: ");
            string lastName = Console.ReadLine();
            Console.Write("Is this person:\n[1] - a customer\n[2] - an employee");
            if(ConsoleHelper.ReadRange(2,1) == 1)
            {
                ConsoleHelper.Confirmation(Rental.Instance.PersonRepository.AddPerson<Customer>(firstName, lastName));
            }
            else
            {
                ConsoleHelper.Confirmation(Rental.Instance.PersonRepository.AddPerson<Employee>(firstName, lastName));
            }
        }

        private void Edit(string description)
        {
            Person person = Rental.Instance.PersonRepository.Persons[_choice - 1];
            if(person is Customer customer)
            {
                Rental.Instance.PersonRepository.ToggleCustomer(customer);
            }
            else if(person is Employee employee)
            {
                Rental.Instance.PersonRepository.ToggleEmployee(employee);
            }
            else
            {
                throw new DerivedClassException($"Derived class {person.GetType()} has to be implemented here.");
            }

            Repeat(description);
        }

        private void Delete(string description)
        {
            Draw();
            string personName = Rental.Instance.PersonRepository.Persons[_choice - 1].FullName;
            Console.WriteLine($"Are you sure you want to delete {personName}?\n\n[1] - Yes!\n[0] - No!");
            if(ConsoleHelper.ReadRange(1) == 1)
            {
                Rental.Instance.PersonRepository.DeletePerson(_choice - 1);
                Console.WriteLine($"\nSuccessfully deleted {personName}.");
                ConsoleHelper.Confirmation();
                _choice = 0;
            }
            else
            {
                Repeat(description);
            }
        }

        protected void Loop(Action<string> action, string description)
        {
            Console.WriteLine(description);
            ConsoleHelper.PrintList<Person>(Rental.Instance.PersonRepository.Persons);
            _choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.PersonRepository.Persons.Count);
            while(_choice != 0)
            {
                action(description);
            }
        }


        // Helper functions
        private void Repeat(string description)
        {
            Draw();
            Console.WriteLine(description);
            ConsoleHelper.PrintList<Person>(Rental.Instance.PersonRepository.Persons);
            _choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.PersonRepository.Persons.Count);
        }
    }
}