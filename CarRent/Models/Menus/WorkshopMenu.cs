using System;
using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Persons;
using CarRent.Models.Vehicles;
using CarRent.Models.Helper;

namespace CarRent.Models.Menus
{
    public class WorkshopMenu : Menu
    {
        public WorkshopMenu() : base("Workshop Menu")
        {
            _options = new List<string>()
            {
                "[1] - Select free Employee",
                "[2] - Drive Vehicle",
                "[3] - Clean Vehicle",
                "[4] - Repair Vehicle",
                "[0] - Return to Main Menu"
            };
            _actions = new List<Action>()
            {
                () => Exit(),
                () => SelectEmployee(),
                () => {DriveVehicle(); Rental.Instance.SaveData();},
                () => {CleanVehicle(); Rental.Instance.SaveData();},
                () => {RepairVehicle(); Rental.Instance.SaveData();}
            };
        }

        // Fields & Properties
        private Employee _currentEmployee;

        // Methods
        private void Exit()
        {
            _currentEmployee = null;
            Navigation.IsFinished = true;
        }

        private void SelectEmployee()
        {
            Console.WriteLine("Choose a free employee or [0] to stop:");
            List<Employee> freeStaff = Rental.Instance.PersonRepository.Employees.Where(e => e.IsFree).ToList();
            ConsoleHelper.PrintList<Employee>(freeStaff);
            int choice = ConsoleHelper.ReadLineRange((uint)freeStaff.Count);
            if(choice == 0)
            {
                return;
            }
            _currentEmployee = freeStaff[choice -1];
        }

        private void DriveVehicle()
        {
            if(_currentEmployee == null)
            {
                Console.WriteLine("Please select an eployee first... Press any key to continue..");
                Console.ReadKey(true);
                return;
            }

            Console.WriteLine("Please choose a vehicle to drive or [0] to stop:\n");
            ConsoleHelper.PrintList<Vehicle>(Rental.Instance.VehicleRepository.Vehicles);
            int choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.VehicleRepository.Vehicles.Count);
            while(choice != 0)
            {
                Draw();
                switch(Rental.Instance.VehicleRepository.Vehicles[choice - 1].DriveAround())
                {
                    case null:
                        Console.WriteLine($"{_currentEmployee.FirstName} crashed and " +
                            $"{Rental.Instance.VehicleRepository.Vehicles[choice - 1].Name} was damaged and got dirty.");
                        break;
                    case false:
                        Console.WriteLine($"{_currentEmployee.FirstName} drove through some mud and " +
                            $"{Rental.Instance.VehicleRepository.Vehicles[choice - 1].Name} got dirty.");
                        break;
                    case true:
                        Console.WriteLine($"Nothing happened while {_currentEmployee.FirstName} " +
                            $"drove {Rental.Instance.VehicleRepository.Vehicles[choice - 1].Name}.");
                        break;
                }
                ConsoleHelper.Confirmation();

                Draw();
                Console.WriteLine("Please choose a vehicle to drive or [0] to stop:\n");
                ConsoleHelper.PrintList<Vehicle>(Rental.Instance.VehicleRepository.Vehicles);
                choice = ConsoleHelper.ReadLineRange((uint)Rental.Instance.VehicleRepository.Vehicles.Count);
            }
        }

        private void RepairVehicle()
        {
            if(_currentEmployee == null)
            {
                Console.WriteLine("Please select an eployee first... Press any key to continue..");
                Console.ReadKey(true);
                return;
            }

            List<Vehicles.Vehicle> damagedVehicles = Rental.Instance.VehicleRepository.Vehicles.Where(v => v.IsDamaged).ToList();
            if(damagedVehicles.Count == 0)
            {
                Console.WriteLine("No vehicles are damaged... Press any key to continue...");
                Console.ReadKey(true);
                return;
            }

            Console.WriteLine("Choose a vehicle or [0] to stop:");
            ConsoleHelper.PrintList<Vehicles.Vehicle>(damagedVehicles);
            int choice = ConsoleHelper.ReadLineRange((uint)damagedVehicles.Count);
            if(choice == 0)
            {
                return;
            }

            Draw();
            if(_currentEmployee.RepairVehicle(damagedVehicles[choice - 1]))
            {
                Console.WriteLine($"{_currentEmployee.FirstName} sucessfully repaired {damagedVehicles[choice - 1].Name}");
            }
            else
            {
                Console.WriteLine($"{damagedVehicles[choice - 1].Name} was not damaged in the first place");
            }
            ConsoleHelper.Confirmation();
        }
        private void CleanVehicle()
        {
            if(_currentEmployee == null)
            {
                Console.WriteLine("Please select an eployee first... Press any key to continue..");
                Console.ReadKey(true);
                return;
            }

            List<Vehicles.Vehicle> dirtyVehicles = Rental.Instance.VehicleRepository.Vehicles.Where(v => v.IsDirty).ToList();
            if(dirtyVehicles.Count == 0)
            {
                Console.WriteLine("No vehicles are dirty... Press any key to continue...");
                Console.ReadKey(true);
                return;
            }

            Console.WriteLine("Choose a vehicle or [0] to stop:");
            ConsoleHelper.PrintList<Vehicles.Vehicle>(dirtyVehicles);
            int choice = ConsoleHelper.ReadLineRange((uint)dirtyVehicles.Count);
            if(choice == 0)
            {
                return;
            }

            Draw();
            if(_currentEmployee.CleanVehicle(dirtyVehicles[choice - 1]))
            {
                Console.WriteLine($"{_currentEmployee.FirstName} sucessfully cleaned {dirtyVehicles[choice - 1].Name}");
            }
            else
            {
                Console.WriteLine($"{dirtyVehicles[choice - 1].Name} was not dirty in the first place");
            }
            ConsoleHelper.Confirmation();
        }

        // Override functions
        public override void Draw()
        {
            Console.Clear();
            DrawBorder('╔', '╗');
            DrawTitle();
            DrawBorder('╠', '╣');
            foreach(string option in _options)
            {
                DrawLine(option);
            }
            if(_currentEmployee != null)
            {
                DrawBorder('╠', '╣');
                DrawLine($"Employee: {_currentEmployee.FullName}");
            }
            DrawBorder('╚', '╝');
        }
    }
}