using System;
using System.Linq;
using System.Collections.Generic;
using CarRent.Models.Administration;
using CarRent.Models.Helper;

namespace CarRent.Models.Menus
{
    public class StatisticsMenu : Menu
    {
        public StatisticsMenu() : base("Statistics Menu")
        {
            _options = new List<string>()
            {
                "[1] - Sales today",
                "[2] - Sales last week",
                "[3] - Sales last month",
                "[4] - Sales last year",
                "[0] - Return to Main Menu"
            };
            _actions = new List<Action>()
            {
                () => Navigation.IsFinished = true,
                () => Sales(DateTime.Now.Date, "Today"),
                () => Sales(DateTime.Now.Date.AddDays(-7), "Last week"),
                () => Sales(DateTime.Now.Date.AddMonths(-1), "Last month"),
                () => Sales(DateTime.Now.Date.AddYears(-1), "Last year")
            };
        }
        private void Sales(DateTime startDate, string range)
        {
            List<Reservation> reservations = Rental.Instance.ReservationRepository.Reservations.Where(r => r.CreationDate >= startDate).ToList();
            int profit = reservations.Sum(r => r.Price);
            Console.WriteLine($"{range} we had {reservations.Count} new reservations.");
            Console.WriteLine($"Theese reservations made us a profit of {profit}.-\n");
            ConsoleHelper.Confirmation();
        }
    }
}