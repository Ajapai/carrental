namespace CarRent.Exceptions
{
    public class DerivedClassException: System.Exception
    {
        public DerivedClassException() : base() { }
        public DerivedClassException(string message) : base(message) { }
        public DerivedClassException(string message, System.Exception inner) : base(message, inner) { }
        protected DerivedClassException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}