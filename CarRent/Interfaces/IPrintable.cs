namespace CarRent.Interfaces
{
    public interface IPrintable
    {
        string DisplayName {get;}
    }
}