﻿using System;
using CarRent.Models;
using System.IO;
using CarRent.Models.Helper;
using CarRent.Exceptions;
using System.Collections.Generic;

namespace CarRent
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Loading data...");
            Rental.Instance.LoadData();
            Rental.Instance.PersonRepository.SetAllFree();
            while(true)
            {
                try
                {
                    Navigation.MainMenu.Draw();
                    Navigation.Execute(Navigation.MainMenu.GetCallback(ConsoleHelper.ReadRange(5)));
                }
                catch(Exception ex)
                {
                    DateTime now = DateTime.Now;
                    string filePath;

                    // Stops the application because certain code about derifed classes is missing.
                    if(ex is DerivedClassException dcex)
                    {
                        filePath = $"LogFiles/Exceptions/DerivedClassException_Log_{now.Day}-{now.Month}-{now.Year}.txt";
                        LogException(dcex, filePath, now);
                        Console.Write("Please implement all derived classes correctly -> See log for more information.");
                        ConsoleHelper.Confirmation();
                        Environment.Exit(0);
                    }

                    // Other exceptions get logged and the programm reloads/resets all important data.
                    filePath = $"LogFiles/Exceptions/Exception_Log_{now.Day}-{now.Month}-{now.Year}.txt";
                    Console.Write("Exception occured -> See logfile for more information.");
                    LogException(ex, filePath, now);
                    ConsoleHelper.Confirmation();
                    Console.WriteLine("Loading data...");
                    Rental.Instance.Reload();
                }
            }
        }

        static void LogException(Exception ex, string filePath, DateTime now)
        {
            using( StreamWriter writer = new StreamWriter( filePath, true ) )
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine($"Date : {now.ToString()}");
                writer.WriteLine();

                while( ex != null )
                {
                    writer.WriteLine( ex.GetType().FullName );
                    writer.WriteLine( "Message : " + ex.Message );
                    writer.WriteLine( "StackTrace : " + ex.StackTrace );

                    ex = ex.InnerException;
                }
            }
        }
    }
}
